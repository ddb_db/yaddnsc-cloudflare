/*
yaddnsc-cloudflare
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package yaddnsccf

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"testing"

	"github.com/cloudflare/cloudflare-go"
	"github.com/stretchr/testify/assert"
	"github.com/vargspjut/wlog"
)

const defaultIP = "100.0.0.0"
const envToken = "CF_API_TOKEN"
const envZoneID = "CF_ZONEID"
const envHost = "CF_HOST"

var api *cloudflare.API
var token string
var zoneID string
var host string

func init() {
	zoneID = os.Getenv(envZoneID)
	host = os.Getenv(envHost)
	token = os.Getenv(envToken)
	cf, err := cloudflare.NewWithAPIToken(token)
	if err != nil {
		panic(err)
	}
	api = cf
}

func TestNoChange(t *testing.T) {
	resetDNS(host)
	req := request{
		Token:  token,
		ZoneID: zoneID,
		Hosts:  []string{host},
	}
	sut := New(wlog.DefaultLogger())
	enc, err := sut.Execute(mustEncode(req), net.ParseIP(defaultIP))
	assert.Nil(t, err)
	result := mustDecode(enc)
	assert.Equal(t, 1, len(result))
	assert.Equal(t, "NoChg", result[0].Status)
}

func TestChange(t *testing.T) {
	resetDNS(host)
	req := request{
		Token:  token,
		ZoneID: zoneID,
		Hosts:  []string{host},
	}
	sut := New(wlog.DefaultLogger())
	enc, err := sut.Execute(mustEncode(req), net.ParseIP("100.50.50.50"))
	assert.Nil(t, err)
	result := mustDecode(enc)
	assert.Equal(t, 1, len(result))
	assert.Equal(t, "Ok", result[0].Status)
}

func TestErr(t *testing.T) {
	resetDNS(host)
	req := request{
		Token:  token,
		ZoneID: zoneID,
		Hosts:  []string{host},
	}
	sut := New(wlog.DefaultLogger())
	enc, err := sut.Execute(mustEncode(req), net.ParseIP("299.400.15.633")) // invalid ip must cause Err response
	assert.Nil(t, err)
	result := mustDecode(enc)
	assert.Equal(t, 1, len(result))
	assert.Equal(t, "Err", result[0].Status)
}

func TestNilSystemIPVarConformsToSchema(t *testing.T) {
	result := buildVarsMap(statusError, "foo", "bar", net.ParseIP(unknownIP), nil)
	assert.Equal(t, unknownIP, result["UpdatedIP"])
}

func TestNilDNSIPVarConformsToSchema(t *testing.T) {
	result := buildVarsMap(statusError, "foo", "bar", nil, net.ParseIP(unknownIP))
	assert.Equal(t, unknownIP, result["OriginalIP"])
}

func TestInvalidSystemIPConformsToSchema(t *testing.T) {
	result := buildVarsMap(statusError, "foo", "bar", net.ParseIP(unknownIP), net.ParseIP("266.0.0.0"))
	assert.Equal(t, unknownIP, result["UpdatedIP"])
}

func TestInvalidDNSIPConformsToSchema(t *testing.T) {
	result := buildVarsMap(statusError, "foo", "bar", net.ParseIP("::g"), net.ParseIP(unknownIP))
	assert.Equal(t, unknownIP, result["OriginalIP"])
}

func mustDecode(input []byte) response {
	resp := response{}
	err := json.Unmarshal(input, &resp)
	if err != nil {
		panic(err)
	}
	return resp
}

func mustEncode(input any) []byte {
	enc, err := json.Marshal(input)
	if err != nil {
		panic(err)
	}
	return enc
}

func resetDNS(host string) {
	results, info, err := api.ListDNSRecords(context.Background(), cloudflare.ZoneIdentifier(zoneID), cloudflare.ListDNSRecordsParams{
		Type: "A",
		Name: host,
	})
	if err != nil {
		panic(err)
	}
	if info.Count != 1 {
		panic(fmt.Errorf("Expecting 1 DNS record match, got %d", info.Count))
	}
	result := results[0]
	if result.Content != defaultIP {
		if r, err := api.UpdateDNSRecord(context.Background(), cloudflare.ZoneIdentifier(zoneID), cloudflare.UpdateDNSRecordParams{
			ID:      result.ID,
			Content: defaultIP,
			Type:    result.Type,
		}); err != nil {
			panic(err)
		} else if r.Content != defaultIP {
			panic(fmt.Errorf("DNS reset failed"))
		}
	}
}
