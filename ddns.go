/*
yaddnsc-cloudflare
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Package yaddnsccf implements support for Cloudflare DNS in yaddnsc
package yaddnsccf

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"net"

	"github.com/cloudflare/cloudflare-go"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc-cloudflare/internal/cfdns"
)

const (
	unknownIP     = "0.0.0.0"
	statusOk      = "Ok"
	statusNoChg   = "NoChg"
	statusUpdated = "Updated"
	statusError   = "Err"
)

//go:embed schema.json
var schema []byte

type Processor struct {
	log wlog.Logger
}

type request struct {
	Token  string
	ZoneID string `json:"zone_id"`
	Hosts  []string
}

type responseEntry struct {
	Details     map[string]any `json:"details,omitempty"`
	Description string         `json:"description"`
	Status      string         `json:"status"`
}

type response []responseEntry

func New(log wlog.Logger) *Processor {
	return &Processor{
		log: log,
	}
}

func (impl *Processor) Init() error {
	return nil
}

func (impl *Processor) Describe() string {
	return "Cloudflare DNS (https://cloudflare.com)"
}

func (impl *Processor) Schema() []byte {
	return schema
}

func (impl *Processor) Execute(blob []byte, systemIP net.IP) ([]byte, error) {
	ddns := request{}
	if err := json.Unmarshal(blob, &ddns); err != nil {
		return nil, fmt.Errorf("yaddnsc-cloudflare: unmarshal failed: %w", err)
	}

	cf, err := cfdns.New(ddns.Token)
	if err != nil {
		return nil, fmt.Errorf("yaddnsc-cloudflare: cfdns init failed: %w", err)
	}

	resp := make(response, 0)
	for _, h := range ddns.Hosts {
		resp = append(resp, impl.executeSingleHost(h, systemIP, ddns, cf))
	}
	return json.Marshal(resp)
}

func (impl *Processor) executeSingleHost(host string, systemIP net.IP, ddns request, cf cfdns.CloudflareDNS) responseEntry {
	ipType := "IPv4"
	if systemIP.To4() == nil {
		ipType = "IPv6"
	}
	desc := fmt.Sprintf("host=%s;zoneId=%s;type=%s", host, ddns.ZoneID, ipType)
	impl.log = impl.log.WithScope(wlog.Fields{"desc": desc})

	resp := responseEntry{
		Description: desc,
	}

	res, err := getDNSRecord(host, ddns.ZoneID, systemIP.To4() != nil, cf)
	if err != nil {
		impl.log.WithScope(wlog.Fields{"err": err.Error()}).Error("fetch dns record failed")
		resp.Status = statusError
		resp.Details = buildVarsMap(statusError, ddns.ZoneID, host, net.ParseIP(unknownIP), systemIP)
		return resp
	}

	dnsIP := net.ParseIP(res.Content)
	if dnsIP == nil {
		impl.log.WithScope(wlog.Fields{"err": err.Error(), "data": res.Content}).Error("IP in DNS is invalid")
		resp.Status = statusError
		resp.Details = buildVarsMap(statusError, ddns.ZoneID, host, net.ParseIP(unknownIP), systemIP)
		return resp
	}

	if dnsIP.Equal(systemIP) {
		resp.Details = buildVarsMap(statusNoChg, ddns.ZoneID, host, dnsIP, systemIP)
		resp.Status = statusNoChg
	} else {
		res.Content = systemIP.String()
		if _, err := cf.UpdateRecord(&res); err != nil {
			impl.log.WithScope(wlog.Fields{"err": err.Error()}).Error("DNS update failed")
			resp.Status = statusError
			resp.Details = buildVarsMap(statusError, ddns.ZoneID, host, dnsIP, systemIP)
			return resp
		}
		resp.Details = buildVarsMap(statusOk, ddns.ZoneID, host, dnsIP, systemIP)
		resp.Status = statusOk
	}
	return resp
}

func getDNSRecord(host, zoneID string, isV4 bool, dns cfdns.CloudflareDNS) (cloudflare.DNSRecord, error) {
	var r *cloudflare.DNSRecord
	var err error
	resType := "A"

	if isV4 {
		r, err = dns.GetARecord(host, zoneID)
	} else {
		resType = "AAAA"
		r, err = dns.GetQuadARecord(host, zoneID)
	}
	if err != nil {
		return cloudflare.DNSRecord{}, err
	}

	if r == nil {
		return cloudflare.DNSRecord{}, fmt.Errorf("no %s record found for %s, you need to create one first", resType, host)
	}
	return *r, nil
}

func buildVarsMap(status, zoneID, host string, origIP, updatedIP net.IP) map[string]any {
	origStr := unknownIP
	if origIP != nil {
		origStr = origIP.String()
	}

	updatedStr := unknownIP
	if updatedIP != nil {
		updatedStr = updatedIP.String()
	}

	return map[string]any{
		"OriginalIP": origStr,
		"UpdatedIP":  updatedStr,
		"Status":     status,
		"ZoneID":     zoneID,
		"Host":       host,
	}
}
