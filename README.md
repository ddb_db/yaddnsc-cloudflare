# yaddnsc-cloudflare: Cloudflare DNS Support for yaddnsc
This module implements support for Cloudflare DNS into [yaddnsc](https://gitlab.com/ddb_db/yaddnsc).

## Features
* Support for IPv4 and IPv6
* Support for multiple host updates

## Requirements
* The DNS record to be updated must already exist in Cloudflare; this module does not create DNS entries.
* You must create and use a scoped Cloudflare token with this module.

## Configuration
Refer to the formal JSON schema definition in `schema.json`.  An example configuration is provided below.

### Example
```
{
    "services":
        [
            {
                "type": "cloudflare",
                "config": {
                    "token": "My-Secret-Cloudflare-Token",
                    "zone_id": "My-DNS-Zone-ID",
                    "hosts": ["myhost.mydomain.com", "mydomain.com"]
                }
            }
        ]
}
```

If you need to update the root record of a domain, use the domain name as shown in the example above.

The type of record (A or AAAA) to be updated is determined by the IP address received in the request from
`yaddnsc`.  When `yaddnsc` sends an IPv4 address then this module attempts to pull the A record for the 
given host and update it.  Same with an IPv6 address.

## Response
As part of a `yaddnsc` response, a service must return a JSON object that is then used for template substitution in
your webhook configurations.  This service's JSON object includes the following keys:

| Key           | Values|
| ---           | ---   |
| OriginalIP    | The IP address found in the DNS record |
| UpdatedIP     | The IP address now stored in DNS; might be same as `OriginalIP` if the IP address did not change. |
| Status        | The `yaddnsc` status for this request; one of `NoChg`, `Ok`, `Err` |
| ZoneID        | The Cloudflare Zone ID being modified. |
| Host          | The hostname being modified or the domain name if it's a root record update; note that this is always a single value as configs with multiple hosts are broken up into multiple requests. |

Use this values for substitutions in your webhooks to send contextual information about an update to your webhooks.

## Contributing
Pull requests are welcome.  Bugs and feature requests can also be submitted as tickets in the project.