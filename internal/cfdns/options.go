/*
yaddnsc-cloudflare
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package cfdns

import (
	"github.com/cloudflare/cloudflare-go"
	"github.com/vargspjut/wlog"
)

type Option func(*defaultImpl)

func WithAPI(api *cloudflare.API) Option {
	return func(impl *defaultImpl) {
		wlog.Debug("cfdns: custom cf api")
		impl.api = api
	}
}
