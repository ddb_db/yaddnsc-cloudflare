/*
yaddnsc-cloudflare
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package cfdns

import (
	"errors"
	"testing"

	"github.com/cloudflare/cloudflare-go"
	"github.com/stretchr/testify/assert"
)

func TestProcessRecordsReturnsErrorWhenCFErrors(t *testing.T) {
	dns := defaultImpl{}
	_, err := dns.processRecords(nil, nil, errors.New("__failed__"), "", "", "")
	assert.ErrorContains(t, err, "__failed__")
}

func TestProcessRecordsReturnsErrorWhenDNSQueryReturnsTooManyMatches(t *testing.T) {
	dns := defaultImpl{}
	_, err := dns.processRecords(make([]cloudflare.DNSRecord, 2), &cloudflare.ResultInfo{Count: 2}, nil, "", "", "")
	assert.ErrorContains(t, err, "Expected 1 or 0 matches")
}

func TestProcessRecordsReturnsNilWhenNoMatchFoundInDNS(t *testing.T) {
	dns := defaultImpl{}
	r, err := dns.processRecords(make([]cloudflare.DNSRecord, 0), &cloudflare.ResultInfo{Count: 0}, nil, "", "", "")
	assert.Nil(t, r)
	assert.Nil(t, err)
}

func TestProcessRecordsReturnsMatch(t *testing.T) {
	dns := defaultImpl{}
	m := cloudflare.DNSRecord{}
	r, err := dns.processRecords([]cloudflare.DNSRecord{m}, &cloudflare.ResultInfo{Count: 1}, nil, "", "", "")
	assert.Equal(t, &m, r)
	assert.Nil(t, err)
}
