/*
yaddnsc-cloudflare
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package cfdns

import (
	"context"
	"fmt"

	"github.com/cloudflare/cloudflare-go"
)

type CloudflareDNS interface {
	GetARecord(name, zoneID string) (*cloudflare.DNSRecord, error)
	GetQuadARecord(name, zoneID string) (*cloudflare.DNSRecord, error)
	UpdateRecord(r *cloudflare.DNSRecord) (*cloudflare.DNSRecord, error)
}

func New(apiToken string, opts ...Option) (CloudflareDNS, error) {
	impl := &defaultImpl{
		token: apiToken,
	}
	for _, o := range opts {
		o(impl)
	}

	if impl.api == nil {
		api, err := impl.buildAPIClient()
		if err != nil {
			return nil, err
		}
		impl.api = api
	}
	return impl, nil
}

type defaultImpl struct {
	token     string
	api       *cloudflare.API
	userAgent string
}

func (impl *defaultImpl) GetARecord(name, zoneID string) (*cloudflare.DNSRecord, error) {
	return impl.getRecord("A", zoneID, name)
}

func (impl *defaultImpl) GetQuadARecord(name, zoneID string) (*cloudflare.DNSRecord, error) {
	return impl.getRecord("AAAA", zoneID, name)
}

func (impl *defaultImpl) UpdateRecord(r *cloudflare.DNSRecord) (*cloudflare.DNSRecord, error) {
	result, err := impl.api.UpdateDNSRecord(context.TODO(), cloudflare.ZoneIdentifier(r.ZoneID), cloudflare.UpdateDNSRecordParams{
		ID:      r.ID,
		Type:    r.Type,
		Content: r.Content,
	})
	return &result, err
}

func (impl *defaultImpl) buildAPIClient() (*cloudflare.API, error) {
	api, err := cloudflare.NewWithAPIToken(impl.token)
	if err != nil {
		return nil, err
	}
	api.UserAgent = fmt.Sprintf("%s %s", impl.userAgent, api.UserAgent)
	return api, nil
}

func (impl *defaultImpl) getRecord(recordType, zoneID, name string) (*cloudflare.DNSRecord, error) {
	records, info, err := impl.api.ListDNSRecords(context.TODO(), cloudflare.ZoneIdentifier(zoneID), cloudflare.ListDNSRecordsParams{
		Type: recordType,
		Name: name,
	})
	return impl.processRecords(records, info, err, recordType, name, zoneID)
}

func (impl *defaultImpl) processRecords(records []cloudflare.DNSRecord, info *cloudflare.ResultInfo, err error, recordType, name, zoneID string) (*cloudflare.DNSRecord, error) {
	if err != nil {
		return nil, fmt.Errorf("cfdns: A record lookup failed: %w", err)
	}
	if info.Count > 1 {
		return nil, fmt.Errorf("cfdns: Expected 1 or 0 matches for '%s', but found %d", name, info.Count)
	}
	if info.Count == 0 {
		return nil, nil
	}
	return &records[0], nil
}
