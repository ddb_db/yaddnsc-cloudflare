{
    "config": {
      "$schema": "https://json-schema.org/draft/2020-12/schema",
      "type": "object",
      "description": "Represents a request for a DDNS update in Cloudflare.",
      "properties": {
        "token": {
          "type": "string",
          "description": "A scoped Cloudflare API token with permissions to modified the given zone id",
          "minLength": 1
        },
        "zone_id": {
          "type": "string",
          "description": "The Cloudflare zone id being modified",
          "minLength": 1
        },
        "hosts": {
          "type": "array",
          "description": "One or more hostnames to be updated; must be FQDN (i.e. myhost.mydomain.com); use domain only for root record (i.e. mydomain.com)",
          "items": {
            "type": "string",
            "format": "idn-hostname",
            "minLength": 3
          },
          "minItems": 1,
          "uniqueItems": true
        }
      },
      "required": [
        "token",
        "zone_id",
        "hosts"
      ],
      "unevaluatedProperties": false
    },
    "response": {
      "$schema": "https://json-schema.org/draft/2020-12/schema",
      "type": "object",
      "description": "Represents a request for a DDNS update in Cloudflare.",
      "properties": {
        "OriginalIP": {
          "$ref": "#/$defs/validIP"
        },
        "UpdatedIP": {
          "$ref": "#/$defs/validIP"
        },
        "Status": {
          "type": "string",
          "description": "The result of the request. One of Err, Ok, NoChg",
          "enum": [
            "Err",
            "Ok",
            "NoChg"
          ]
        },
        "ZoneID": {
          "type": "string",
          "description": "The Cloudflare zone id that is being modified.",
          "minLength": 1
        },
        "Host": {
          "type": "string",
          "description": "The FQDN hostname being modified; for a root record, the domain name is returned.",
          "minLength": 3,
          "format": "idn-hostname",
          "pattern": "^(?:.+\\.)?.+\\..+"
        }
      },
      "required": [
        "OriginalIP",
        "UpdatedIP",
        "Status",
        "ZoneID",
        "Host"
      ],
      "unevaluatedProperties": false,
      "$defs": {
        "validIP": {
          "type": "string",
          "description": "A valid IPv4 or IPv6 address.",
          "minLength": 1,
          "anyOf": [
            {
              "format": "ipv4"
            },
            {
              "format": "ipv6"
            }
          ]
        }
      }
    }
  }